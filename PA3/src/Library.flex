package IC.Parser;
import IC.Parser.Token;

%%

%public
%function next_token
%type Token
%line
%column
%scanerror LexicalError
%class LibraryLexer
%state COMMENTS1
%state COMMENTS2
%cup

%{
  
 /**
 * Return a new Token with the given token id, token value and with the current line and
 * column numbers (index 1).
 */
Token newToken(int tokenId, String tag) {
    return new Token(tokenId, yyline+1, yycolumn+1,tag,yytext());
}

Token newToken(int tokenId) {
    return new Token(tokenId, yyline+1, yycolumn+1,yytext(),yytext());
}

public int getLineNumber()
{
    	return yyline+1;
}

public int getColumnNumber()
{
    	return yycolumn+1;
}

%}

%eofval{
  	return new Token(Libsym.EOF);
%eofval}


LOWERALPHA = [a-z]
UPPERALPHA = [A-Z]
ALPHA = {UPPERALPHA}| {LOWERALPHA} 
DIGIT = [0-9]
ALPHA_NUMERIC = {ALPHA}|{DIGIT}
ALPHA_NUMERIC_UNDERSCORE = {ALPHA_NUMERIC} | [_]
ID = {LOWERALPHA}({ALPHA_NUMERIC_UNDERSCORE})*
SPACE = [ ] | [\n] | [\r] | [\t]
COMMENT1 = "//"
COMMENT2 = "/*"
%%

<COMMENTS1> {
	[^\n] {}
	[\n] { yybegin(YYINITIAL); }
}

<COMMENTS2> {
  "*/" { yybegin(YYINITIAL); }
  . | {SPACE} {}
  <<EOF>>	{ throw new LexicalError(String.format("%d:%d : Lexical error; unterminated comment", yyline+1, yycolumn+1)); }
}

<YYINITIAL> {
{SPACE}  {}
Library { return newToken(Libsym.LIBRARY); }
class { return newToken(Libsym.CLASS); }
static { return newToken(Libsym.STATIC); }
void { return newToken(Libsym.VOID); }
int { return newToken(Libsym.INT); }
boolean  { return newToken(Libsym.BOOLEAN); }
string { return newToken(Libsym.STRING); }
"," { return newToken(Libsym.COMMA); }
"{" { return newToken(Libsym.LEFT_BRACES); }
"}" { return newToken(Libsym.RIGHT_BRACES); }
"(" { return newToken(Libsym.LEFT_PARENTHESES); }
")" { return newToken(Libsym.RIGHT_PARENTHESES); } 
"[" { return newToken(Libsym.LEFT_SQUARE_BRACKETS); }
"]" { return newToken(Libsym.RIGHT_SQUARE_BRACKETS); }
";" { return newToken(Libsym.SEMICOLON); }
{ID} { return newToken(Libsym.ID,"ID"); }
{COMMENT1} { yybegin(COMMENTS1);}
{COMMENT2} { yybegin(COMMENTS2); }
}

[^] { throw new LexicalError((yyline + 1) + ":" + (yycolumn + 1)+" : Lexical error; unsupported symbol found: \"" +yytext()+"\""); }