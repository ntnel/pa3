package IC.Parser;
import IC.Parser.Token;

%%

%public
%function next_token
%type Token
%line
%column
%scanerror LexicalError
%class Lexer
%state COMMENTS1 COMMENTS2 STRING
%cup

%{

	//For String buffering
	int strLine=0, strColumn=0;
	StringBuffer strBuffer = new StringBuffer();
	
  
Token newStringToken(int tokenId, String value)
    {
    	return new Token(tokenId, strLine, strColumn, "STRING",value );
    }
    
 /**
 * Return a new Token with the given token id, token value and with the current line and
 * column numbers (index 1).
 */
Token newToken(int tokenId, String tag) {
    return new Token(tokenId, yyline+1, yycolumn+1,tag,yytext());
}

Token newToken(int tokenId) {
    return new Token(tokenId, yyline+1, yycolumn+1,yytext(),yytext());
}

public int getLineNumber()
{
    	return yyline+1;
}

public int getColumnNumber()
{
    	return yycolumn+1;
}

%}

%eofval{
  	return new Token(sym.EOF);
%eofval}


LOWERALPHA= [a-z]
UPPERALPHA= [A-Z]
ALPHA={UPPERALPHA}| {LOWERALPHA} 
DIGIT=[0-9]
ALPHA_NUMERIC={ALPHA}|{DIGIT}
ALPHA_NUMERIC_UNDERSCORE={ALPHA_NUMERIC} | [_]
IDENT={LOWERALPHA}({ALPHA_NUMERIC_UNDERSCORE})*
CLASSIDENT= {UPPERALPHA}({ALPHA_NUMERIC_UNDERSCORE})*
NUMBER = {DIGIT}+
STRINGCHAR = [^\r\n\"\\]
SPACE= [ ] | [\n] | [\r] | [\t]
COMMENT1= "//"
COMMENT2= "/*"
%%

 
<COMMENTS1> {
  [^\n] {}
  [\n] { yybegin(YYINITIAL); }
}

<COMMENTS2> {
  "*/" { yybegin(YYINITIAL); }
  . | {SPACE} {}
  <<EOF>>	{ throw new LexicalError(String.format("%d:%d : Lexical error; unterminated comment", yyline+1, yycolumn+1)); }
}

<STRING> {
  \" { yybegin(YYINITIAL); return newStringToken(sym.LITERALSTRING, strBuffer.toString()); }
  \t { throw new LexicalError(String.format("%d:%d : Lexical error; tab character not allowed in string", yyline + 1, yycolumn+1)); }
    
  "\\t" { strBuffer.append( "\t" ); }
  "\\n" { strBuffer.append( "\n" ); }
  "\\r" { strBuffer.append( "\r" ); }
  "\\\"" { strBuffer.append( "\\\"" ); }
  "\\\\" { strBuffer.append( "\\\\" ); }
  {STRINGCHAR}+ { strBuffer.append( yytext() ); }
  
  \\.  { throw new LexicalError(String.format("%d:%d : Illegal escape character \""+yytext()+"\"", yyline+1, yycolumn+1)); }
  \r|\n|\r\n   { throw new LexicalError(String.format("%d:%d : Lexical error; string not terminated at end of line", yyline+1, yycolumn+1)); }
}

<YYINITIAL>{

{SPACE}  {}
 class { return newToken(sym.CLASS); }
 extends { return newToken(sym.EXTENDS); }
 static { return newToken(sym.STATIC); }
 void { return newToken(sym.VOID); }
 int { return newToken(sym.INT); }
 boolean  { return newToken(sym.BOOLEAN); }
 string { return newToken(sym.STRING); }
 return { return newToken(sym.RETURN); }
 if { return newToken(sym.IF); }
 else { return newToken(sym.ELSE); }
 while { return newToken(sym.WHILE); }
 break { return newToken(sym.BREAK); }
 continue { return newToken(sym.CONTINUE); }
 this { return newToken(sym.THIS); }
 new { return newToken(sym.NEW); }
 length { return newToken(sym.LENGTH); }
 true { return newToken(sym.TRUE); }
 false { return newToken(sym.FALSE); }
 null { return newToken(sym.NULL); }
 "." { return newToken(sym.DOT); }
 "," { return newToken(sym.COMMA); }
 "{" { return newToken(sym.LEFT_BRACES); }
 "}" { return newToken(sym.RIGHT_BRACES); }
 "(" { return newToken(sym.LEFT_PARENTHESES); }
 ")" { return newToken(sym.RIGHT_PARENTHESES); }
 "[" { return newToken(sym.LEFT_SQUARE_BRACKETS); }
 "]" { return newToken(sym.RIGHT_SQUARE_BRACKETS); }
 ";" { return newToken(sym.SEMICOLON); }
 "+" { return newToken(sym.PLUS); }
 "-" { return newToken(sym.MINUS); }
 "*" { return newToken(sym.MULTI); }
 "/" { return newToken(sym.DIVISION); }
 "%" { return newToken(sym.MODULO); }
 "&&" { return newToken(sym.AND); }
 "||" { return newToken(sym.OR); }
 "<=" { return newToken(sym.SMALLER_OR_EQUAL); }
 "<" { return newToken(sym.SMALLER); }
 ">=" { return newToken(sym.BIGGER_OR_EQUAL); }
 ">" { return newToken(sym.BIGGER); }
 "==" { return newToken(sym.EQUAL); }
 "!=" { return newToken(sym.NOT_EQUAL); }
 "!" { return newToken(sym.NOT); }
 "=" { return newToken(sym.ASSIGN); }
 {CLASSIDENT} { return newToken(sym.CLASS_ID,"CLASS_ID"); }
 {IDENT} { return newToken(sym.ID,"ID"); }
 {NUMBER} { return newToken(sym.LITERALINTEGER,"INTEGER"); }
 {COMMENT1} { yybegin(COMMENTS1); }
 {COMMENT2} { yybegin(COMMENTS2); }
  \"  { strBuffer.setLength(0); strLine = yyline+1; strColumn = yycolumn + 1; yybegin(STRING);  }
}

[^] { throw new LexicalError((yyline + 1) + ":" + (yycolumn + 1)+" : Lexical error; unsupported symbol found: \"" +yytext()+"\""); }
