package IC.SymbolTable;

public enum Kind {

	CLASS ("Class"), 
	METHOD_STATIC("Static method"), 
	METHOD_VIRTUAL("Virtual method"), 
	STATMENT_BLOCK("Statment block"),
	VARIABLE("Local variable"),
	FIELD("Field"),
	FORMAL("Parameter");
	
	private String description;

	private Kind(String description) 
	{
		this.description = description;
	}


	/**
	 * Returns a description of the kind.
	 * 
	 * @return The description.
	 */
	public String getDescription() 
	{
		return description;
	}
}
