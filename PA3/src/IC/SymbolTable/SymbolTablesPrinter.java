package IC.SymbolTable;

import java.util.List;


public class SymbolTablesPrinter {

	SymbolTable root;
	String fileName;

	public SymbolTablesPrinter(SymbolTable root, String icFileName) {
		this.root = root;
		this.fileName = icFileName;
	}

	public void printTables() {
		printGlobalTable();
		printClasses();

	}

	private void printGlobalTable() {
		System.out.println("\nGlobal Symbol Table: " + fileName);
		for (IC.SymbolTable.Symbol sym : root.getEntries().values())
			System.out.println("    " + sym.getKind().getDescription() + ": "
					+ sym.getId());
		if (root.getChildren().size() > 0)
			printChildrenTables(root);
		else
			System.out.print("\n");

	}

	private void printClasses() {
		for (SymbolTable classTable : root.getChildren()) {
			System.out.println("Class Symbol Table: " + classTable.getId());
			printFieldListing(classTable);
			printMethodListing(classTable);
			if (classTable.getChildren().size() > 0) {
				printChildrenTables(classTable);
				printMethods(classTable);
			} else
				System.out.print("\n");
		}

	}

	private void printFieldListing(SymbolTable classTable) {
		for (IC.SymbolTable.Symbol sym : classTable.getEntries().values())
			if (sym.getKind() == Kind.FIELD)
				System.out.println("    " + sym.getKind().getDescription()
						+ ": " + sym.getType().getDescription() + " " + sym.getId());
	}

	private void printMethodListing(SymbolTable classTable) {
		for (IC.SymbolTable.Symbol sym : classTable.getEntries().values())
			if (sym.getKind() != Kind.FIELD)
				System.out.println("    " + sym.getKind().getDescription()
						+ ": " + sym.getId() + " " + sym.getType().getDescription());
	}

	private void printMethods(SymbolTable papa) {
		for (SymbolTable methodTable : papa.getChildren()) {
			System.out.println("Method Symbol Table: " + methodTable.getId());
			printListing(methodTable,Kind.FORMAL);
			printListing(methodTable, Kind.VARIABLE);
			if (methodTable.getChildren().size() > 0) {
				printChildrenTables(methodTable);
				printBlocks(methodTable);
			} else
				System.out.print("\n");
		}

	}

	private void printListing(SymbolTable methodTable, Kind kindToPrint) {
		for (IC.SymbolTable.Symbol sym : methodTable.getEntries().values())
			if (sym.getKind() == kindToPrint)
				System.out.println("    " + sym.getKind().getDescription()
						+ ": " + sym.getType().getDescription() + " " + sym.getId()
						+ " ");
	}

	private void printBlocks(SymbolTable papa) {
		for (SymbolTable blockTable : papa.getChildren()) {
			String located = "Statement Block Symbol Table ( located in ";
			located += papa.getId() + " )";
			System.out.println(located);
			for (IC.SymbolTable.Symbol sym : blockTable.getEntries().values())
				if (sym.getKind() != Kind.STATMENT_BLOCK)
					System.out.println("    " + sym.getKind().getDescription()
							+ ": " + sym.getType().getDescription() + " "
							+ sym.getId() + " ");
			if (blockTable.getChildren().size() > 0) {
				printChildrenTables(blockTable);
				printBlocks(blockTable);
			} else
				System.out.print("\n");
		}
	}

	private void printChildrenTables(SymbolTable papa) {
		System.out.print("Children tables: ");
		List<SymbolTable> children = papa.getChildren();
		for (int i = 0; i < children.size(); i++) {
			SymbolTable currTable = children.get(i);
			System.out.print(currTable.getId());
			if (i != children.size() - 1)
				System.out.print(", ");

		}
		System.out.println("\n");

	}
}
