package IC.SymbolTable;

public class ClassSymbolTable extends SymbolTable {

	/** map from String to Symbol **/
	private SymbolTable staticEntries;

	public ClassSymbolTable(String id) {
		super(id,tableTypes.CLASS);
		staticEntries = new innerSymbolTable(id,tableTypes.CLASS);
	}


	public SymbolTable getStaticScope() {
		return staticEntries;
	}


	@Override
	public void setParentSymbolTable(SymbolTable parentSymbolTable) {
		if (parentSymbolTable instanceof ClassSymbolTable) {
			ClassSymbolTable parentClassTable = (ClassSymbolTable) parentSymbolTable;
			this.parentSymbolTable=parentClassTable;
			staticEntries.setParentSymbolTable(parentClassTable
					.getStaticScope());
		} else {
			this.parentSymbolTable=parentSymbolTable;
			staticEntries.setParentSymbolTable(parentSymbolTable);
		}
	}
	
	private class innerSymbolTable extends SymbolTable{
		
		public innerSymbolTable(String id, tableTypes type) {
			super(id, type);
		}
		
		@Override
		public void insert(Symbol toInsert) {
			super.insert(toInsert);
			ClassSymbolTable.this.insert(toInsert);
		}
		
		@Override
		public Symbol getSymbol(String id) {
			return ClassSymbolTable.this.getSymbol(id);
		}
		
	}


}
