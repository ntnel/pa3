package IC.SymbolTable;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SymbolTable {

	/** map from String to Symbol **/
	private Map<String, Symbol> entries;
	private String id;
	private tableTypes tableType;
	protected SymbolTable parentSymbolTable;
	private List<SymbolTable> childrenTables;

	public static enum tableTypes {
		GLOBAL, CLASS, METHOD, BLOCK
	};

	public SymbolTable(String id, tableTypes type) {
		this.id = id;
		tableType = type;
		entries = new LinkedHashMap<String, Symbol>();
		childrenTables = new LinkedList<SymbolTable>();
	}

	public Map<String, Symbol> getEntries() {
		return entries;
	}

	public Symbol lookUp(String id) {
		Symbol entry = entries.get(id);
		if (entry != null || parentSymbolTable == null)
			return entry;
		else
			return parentSymbolTable.lookUp(id);
	}

	public void insert(Symbol toInsert) {
		entries.put(toInsert.getId(), toInsert);
	}

	public Symbol getSymbol(String id) {
		return entries.get(id);
	}

	public boolean contains(String symbolId) {
		return entries.get(symbolId) != null;
	}

	public void addChildSymbolTable(SymbolTable child) {
		childrenTables.add(child);
		child.setParentSymbolTable(this);
	}

	public List<SymbolTable> getChildren() {
		return childrenTables;
	}

	public String getId() {
		return id;
	}

	public SymbolTable getParentSymbolTable() {
		return parentSymbolTable;
	}

	public void setParentSymbolTable(SymbolTable parentSymbolTable) {
		this.parentSymbolTable = parentSymbolTable;
	}

	public SymbolTable getChildTableById(String tableId) {
		for (SymbolTable table : childrenTables) {
			if (table.id.equals(tableId))
				return table;
		}
		return null;
	}

	public tableTypes getTableType() {
		return tableType;
	}

}
