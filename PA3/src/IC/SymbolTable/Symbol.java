package IC.SymbolTable;

import IC.Types.Type;


public class Symbol {
	private String id;
	private Type type;
	private Kind kind;

	public Symbol(String id, Kind kind) {
		this.id = id;
		this.kind = kind;
		this.type=null;
	}

	public String getId() {
		return id;
	}

	public Type getType() {
		return type;
	}
	
	public void setType(Type type) {
		this.type = type;
	}

	public Kind getKind() {
		return kind;
	}


}
