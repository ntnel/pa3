package IC.Types;

import IC.AST.icclass.ICClass;

public class ClassType extends Type {

	private ICClass classNode;

	public ClassType(ICClass classNode) {
		super(classNode.getName(),true);
		this.classNode = classNode;
	}

	public ICClass getClassNode() {
		return classNode;
	}

	@Override
	public String toString() {
		String description = classNode.getName();
		if (classNode.hasSuperClass())
			description += String.format(", Superclass ID: %d", classNode
					.enclosingScope().lookUp(classNode.getSuperClassName())
					.getType().getId());
		return getStringRep("Class", description);
	}

	@Override
	public boolean isSubtypeOf(Type t) {
		if (t == null)
			return false;
		if (!(t instanceof ClassType))
			return false;
		if (t == this)
			return true;
		ClassType otherType = (ClassType) t;
		if (classNode.hasSuperClass()) {
			if (classNode.getSuperClassName().equals(
					otherType.getClassNode().getName()))
				return true;
			else
				return classNode.enclosingScope()
						.lookUp(classNode.getSuperClassName()).getType()
						.isSubtypeOf(t);
		}

		return false;
	}

}
