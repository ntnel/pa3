package IC.Types;

import java.util.List;

public class MethodType extends Type {

	List<Type> paramTypes;
	Type returnType;

	public MethodType(List<Type> arguments, Type returnType) {
		super();
		this.paramTypes = arguments;
		this.returnType = returnType;
		String typeName = "{";
		if (arguments.size() > 0) {
			typeName += arguments.get(0).getDescription();
			for (int i = 1; i < arguments.size(); i++)
				typeName += ", " + arguments.get(i).getDescription();
		}
		typeName += " -> ";
		typeName += returnType.getDescription() + "}";
		description = typeName;
	}

	@Override
	public String toString() {
		return getStringRep("Method type");
	}

	public boolean matches(MethodType other) {
		if (other.getReturnType() != returnType)
			return false;
		if (other.getParamTypes().size() != paramTypes.size())
			return false;
		List<Type> otherTypes = other.getParamTypes();
		for (int i = 0; i < paramTypes.size(); i++) {
			if (otherTypes.get(i) != paramTypes.get(i))
				return false;
		}
		return true;
	}

	@Override
	public boolean isSubtypeOf(Type t) {
		return t != null && this == t;
	}

	public List<Type> getParamTypes() {
		return paramTypes;
	}

	public Type getReturnType() {
		return returnType;
	}

}
