package IC.Types;


public class ArrayType extends Type {
	
	Type cellType;

	public ArrayType(Type cellType) {
		super(cellType.getDescription()+"[]",true);
		this.cellType=cellType;
	}
	
	@Override
	public String toString() {
		return getStringRep("Array type");
	}
	
	@Override
	public boolean isSubtypeOf(Type t) {
		return t != null && this == t;
	}

	public Type getCellType() {
		return cellType;
	}

}
