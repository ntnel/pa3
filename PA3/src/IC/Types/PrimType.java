package IC.Types;

public class PrimType extends Type {

	private PrimitiveTypes type;

	public PrimType(PrimitiveTypes type) {
		super(type.getDescription(),type==PrimitiveTypes.STRING);
		this.type = type;
	}

	public PrimitiveTypes getPrimitiveType() {
		return type;
	}

	@Override
	public String toString() {
		return getStringRep("Primitive type");
	}

	@Override
	public boolean isSubtypeOf(Type t) {
		if (type==PrimitiveTypes.NULL && t.isReferenceType())
			return true;
		return t != null && this == t;
	}

}
