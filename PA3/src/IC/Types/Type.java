package IC.Types;

public abstract class Type {

	protected int id;
	private static int idGen = 0;
	protected String description;
	private boolean referenceType;

	public Type() {
		id = ++idGen;
	}

	public Type(String name,boolean referenceType) {
		this();
		this.description = name;
		this.referenceType=referenceType;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
	
	public abstract boolean isSubtypeOf(Type t);

	protected String getStringRep(String type) {
		return getStringRep(type, getDescription());
	}

	protected String getStringRep(String type, String desc) {
		return String.format("%d: %s: %s\n", getId(), type, desc);
	}
	
	public boolean isReferenceType(){
		return referenceType;
	}

}
