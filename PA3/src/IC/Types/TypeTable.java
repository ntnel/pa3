package IC.Types;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import IC.AST.icclass.ICClass;

public class TypeTable {

	private static TypeTable instance;

	private Map<String, PrimType> primTypes;
	private Map<String, ClassType> classTypes;
	private Map<Type, ArrayType> arrayTypes;
	private Map<List<Type>, MethodType> methodTypes;

	private TypeTable() {
		primTypes = new LinkedHashMap<String, PrimType>();
		classTypes = new LinkedHashMap<String, ClassType>();
		arrayTypes = new LinkedHashMap<Type, ArrayType>();
		methodTypes = new LinkedHashMap<List<Type>, MethodType>();

		PrimType integerType = new PrimType(PrimitiveTypes.INT);
		PrimType booleanType = new PrimType(PrimitiveTypes.BOOLEAN);
		PrimType nullType = new PrimType(PrimitiveTypes.NULL);
		PrimType stringType = new PrimType(PrimitiveTypes.STRING);
		PrimType voidType = new PrimType(PrimitiveTypes.VOID);
		primTypes.put(integerType.getDescription(), integerType);
		primTypes.put(booleanType.getDescription(), booleanType);
		primTypes.put(nullType.getDescription(), nullType);
		primTypes.put(stringType.getDescription(), stringType);
		primTypes.put(voidType.getDescription(), voidType);

		LinkedList<Type> mainMethodArgs = new LinkedList<Type>();
		mainMethodArgs.add(getArrayType(getPrimType("string")));
		MethodType mainMethodType = new MethodType(mainMethodArgs,
				getPrimType("void"));
		methodTypes.put(mainMethodArgs, mainMethodType);
	}

	public static TypeTable getInstance() {
		if (instance == null)
			instance = new TypeTable();
		return instance;
	}

	public PrimType getPrimType(String name) {
		return primTypes.get(name);

	}

	public ClassType getClassType(ICClass classNode) {
		ClassType classType = classTypes.get(classNode.getName());
		// Class type doesn't exist, need to create it
		if (classType == null) {
			classType = new ClassType(classNode);
			classTypes.put(classNode.getName(), classType);
		}
		return classType;
	}

	public MethodType getMethodType(List<Type> argTypes, Type returnType) {
		LinkedList<Type> types = new LinkedList<>();
		types.add(returnType);
		types.addAll(argTypes);
		MethodType methodType = methodTypes.get(types);
		// Class type doesn't exist, need to create it
		if (methodType == null) {
			methodType = new MethodType(argTypes, returnType);
			methodTypes.put(types, methodType);
		}
		return methodType;

	}

	public ArrayType getArrayType(Type cellType) {
		ArrayType arrType = arrayTypes.get(cellType);
		// Array type doesn't exist, need to create it
		if (arrType == null) {
			arrType = new ArrayType(cellType);
			arrayTypes.put(cellType, arrType);
		}
		return arrType;

	}

	public static boolean isMainMethodTypeStructure(MethodType method) {
		boolean correctReturnType = method.getReturnType() == instance
				.getPrimType("void");
		boolean correctParamTypes = method.getParamTypes().size() == 1
				&& method.getParamTypes().get(0) == instance
						.getArrayType(instance.getPrimType("string"));
		return correctReturnType && correctParamTypes;
	}

	@Override
	public String toString() {
		StringBuilder buffer = new StringBuilder();
		appendType(buffer, primTypes.values());
		appendType(buffer, classTypes.values());
		appendType(buffer, arrayTypes.values());
		appendType(buffer, methodTypes.values());
		return buffer.toString();
	}

	private <T extends Type> void appendType(StringBuilder buffer,
			Collection<T> values) {
		for (Type type : values)
			buffer.append("    " + type.toString());
	}

}
