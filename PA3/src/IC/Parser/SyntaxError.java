package IC.Parser;

public class SyntaxError extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8406649969904302205L;
	public String msg;

	public SyntaxError(String message) {
		msg = message;
	}
}
