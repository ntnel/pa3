package IC.Parser;

public class LexicalError extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8406649969904302205L;
	public String msg;

	public LexicalError(String message) {
		msg = message;
	}
}
