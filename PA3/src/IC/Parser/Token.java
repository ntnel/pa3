package IC.Parser;

import java_cup.runtime.Symbol;

public class Token extends Symbol {

	private int tag,line, column;
	private String tagAsString,value;
	
	public Token(int id){
		super(id,null);
	}
	
	public Token(int id, int line, int column, String tag, String value) {
		super(id, null);
		this.line = line;
		this.column = column;
		this.tagAsString=tag;
		this.tag = id;
		this.value = value;
		super.value=value;
	}
	
	public int getId(){
		return tag;
	}

	public int getLine() {
		return line;
	}

	public int getColumn() {
		return column;
	}

	public String getTag() {
		return tagAsString;
	}

	public String getValue() {
		return value;
	}

	public void print() {
		PrintToken(value, tagAsString, line, column);
	}

	public static void PrintToken(String token, String tag, int line, int column) {
		System.out.println(token + "\t" + tag + "\t" + line + ":" + column);
	}
}
