package IC;

import java.io.FileNotFoundException;
import java.io.FileReader;

import java_cup.runtime.Symbol;
import IC.AST.icclass.ICClass;
import IC.AST.program.Program;
import IC.Parser.Lexer;
import IC.Parser.LexicalError;
import IC.Parser.LibParser;
import IC.Parser.LibraryLexer;
import IC.Parser.Parser;
import IC.Parser.SyntaxError;
import IC.SemanticChecks.ScopeRulesVerifier;
import IC.SemanticChecks.SemanticError;
import IC.SemanticChecks.SymbolTablesBuilder;
import IC.SemanticChecks.TypeRulesVerifyier;
import IC.SemanticChecks.Test.SemanticChecksTester;
import IC.SymbolTable.SymbolTable;

/**
 * @team BenGay <ntnel6@gmail.com> 1. 201192242 2. 200278778 3. 200346575
 */
public class Compiler {
	public static void main(String[] args) {
		if (args.length == 1) {
			doRun(args);	
		} else {
			SemanticChecksTester.ExecuteTestsMain();
		}
	}

	public static void doRun(String[] args) {
		try {
			run(args);
		} catch (FileNotFoundException e) {
			throw new RuntimeException("IO Error (brutal exit)" + e.toString());
		} catch (LexicalError e) {
			System.out.println(e.msg);
		} catch (SyntaxError e) {
			System.out.println(e.msg);
		} catch (SemanticError e) {
			System.out.println(e.msg);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void run(String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("Please insert .txt input file path");
			return;
		}

		String icFilePath = args[0];
		String libFilePath = args.length == 2 ? args[1].substring(2) : System
				.getProperty("user.dir") + "\\libic.sig";
		String[] icFilePathArr = icFilePath.split("\\\\");
		String icFileName = icFilePathArr[icFilePathArr.length - 1];
		String[] libFilePathArr = libFilePath.split("\\\\");
		String libFileName = libFilePathArr[libFilePathArr.length - 1];

		// Try parsing library
		ICClass libClass = parseLibrary(libFilePath);
		System.out.println(String
				.format("Parsed %s successfully!", libFileName));

		// Try parsing code
		Program root = parseCode(icFilePath);
		System.out
				.println(String.format("Parsed %s successfully!\n", icFileName));

		// Combine code and library
		root.getClasses().add(0, libClass);

		// System.out.println((String) root.accept(new
		// PrettyPrinter(icFileName)));
		
		//Build symbol table and bind user defined types to all symbols
		SymbolTable globalScope = new SymbolTablesBuilder().build(root);
		
		//Check scope rules and verify class hierarchy
		new ScopeRulesVerifier().verify(root,globalScope);
		
		//Check type rules
		new TypeRulesVerifyier().verify(root,globalScope);
		
		
		//new SymbolTablesPrinter(globalScope, icFileName).printTables();
		//System.out.println(String.format("Type Table: %s", icFileName));
		//System.out.println(TypeTable.getInstance().toString());
		
		

		
		
		//Resolve all user types and attach type to all symbols
		

	}

	private static Program parseCode(String fileName) throws Exception {
		FileReader txtFile = new FileReader(fileName);
		Symbol o = new Parser(new Lexer(txtFile)).parse();
		Program root = (Program) o.value;
		return root;
	}

	private static ICClass parseLibrary(String fileName) throws Exception {
		FileReader txtFile = new FileReader(fileName);
		Program lib = (Program) new LibParser(new LibraryLexer(txtFile))
				.parse().value;
		return lib.getClasses().get(0);
	}

}