package IC.AST.program;

import java.util.LinkedList;
import java.util.List;

import IC.AST.ASTNode;
import IC.AST.Visitor;
import IC.AST.icclass.ICClass;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * Root AST node for an IC program.
 */
public class Program extends ASTNode 
{

	private List<ICClass> classes;

	public Object accept(Visitor visitor) 
	{
		return visitor.visit(this);
	}
	
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context) throws SemanticError  {
		return v.visit(this, context);
		
	}

	/**
	 * Constructs a new program node.
	 * 
	 * @param classes
	 *            List of all classes declared in the program.
	 */
	public Program(List<ICClass> classes) 
	{
		super(0);
		this.classes = classes;
	}
	
	public Program(ICClass singleClass) 
	{
		super(0);
		this.classes=new LinkedList<ICClass>();
		this.classes.add(singleClass);
	}

	public List<ICClass> getClasses() 
	{
		return classes;
	}
	
	public ICClass getClass(String className){
		for (ICClass c:classes){
			if (c.getName().equals(className))
				return c;
		}
		return null;
	}

}
