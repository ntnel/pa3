package IC.AST;

import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;
import IC.SymbolTable.SymbolTable;
import IC.Types.Type;

/**
 * Abstract AST node base class.
 */
public abstract class ASTNode {
	private int line;
	private Type nodeType;

	/** reference to symbol table of enclosing scope **/
	protected SymbolTable enclosingScope;

	/**
	 * Double dispatch method, to allow a visitor to visit a specific subclass.
	 * 
	 * @param visitor
	 *            - The visitor.
	 * @return A value propagated by the visitor.
	 */
	public abstract Object accept(Visitor visitor);

	public abstract void accept(ExceptionVisitor visitor) throws SemanticError;

	/** accept propagating visitor **/
	public abstract <D, U> U accept(PropagatingVisitor<D, U> v, D context)
			throws SemanticError;

	/**
	 * Constructs an AST node corresponding to a line number in the original
	 * code. Used by subclasses.
	 * 
	 * @param line
	 *            - The line number.
	 */
	protected ASTNode(int line) {
		this.line = line;
	}

	public int getLine() {
		return line;
	}

	/** returns symbol table of enclosing scope **/
	public SymbolTable enclosingScope() {
		return enclosingScope;
	}

	public void setEnclosingScope(SymbolTable enclosingScope) {
		this.enclosingScope = enclosingScope;
	}

	public SymbolTable getClassEnclosingScope() {
		SymbolTable currTable = enclosingScope;
		while (currTable.getTableType() != SymbolTable.tableTypes.CLASS
				&& currTable != null)
			currTable = currTable.getParentSymbolTable();
		return currTable;
	}

	public Type getNodeType() {
		return nodeType;
	}

	public void setNodeType(Type nodeType) {
		this.nodeType = nodeType;
	}
}