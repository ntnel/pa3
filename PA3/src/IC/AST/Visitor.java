package IC.AST;

import IC.AST.expression.ArrayLocation;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;

/**
 * AST visitor interface. Declares methods for visiting each type of AST node.
 */
public interface Visitor 
{
	public Object visit(Program program);

	public Object visit(ICClass icClass);

	public Object visit(Field field);

	public Object visit(VirtualMethod method);

	public Object visit(StaticMethod method);

	public Object visit(LibraryMethod method);

	public Object visit(Formal formal);

	public Object visit(PrimitiveType type);

	public Object visit(UserType type);

	public Object visit(Assignment assignment);

	public Object visit(CallStatement callStatement);

	public Object visit(Return returnStatement);

	public Object visit(If ifStatement);

	public Object visit(While whileStatement);

	public Object visit(Break breakStatement);

	public Object visit(Continue continueStatement);

	public Object visit(StatementsBlock statementsBlock);

	public Object visit(LocalVariable localVariable);

	public Object visit(VariableLocation location);

	public Object visit(ArrayLocation location);

	public Object visit(StaticCall call);

	public Object visit(VirtualCall call);

	public Object visit(This thisExpression);

	public Object visit(NewClass newClass);

	public Object visit(NewArray newArray);

	public Object visit(Length length);

	public Object visit(MathBinaryOp binaryOp);

	public Object visit(LogicalBinaryOp binaryOp);

	public Object visit(MathUnaryOp unaryOp);

	public Object visit(LogicalUnaryOp unaryOp);

	public Object visit(Literal literal);

	public Object visit(ExpressionBlock expressionBlock);
}
