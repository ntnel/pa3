package IC.AST.icclass;

import java.util.LinkedList;
import java.util.List;

import IC.AST.ASTNode;
import IC.AST.Visitor;
import IC.AST.field.Field;
import IC.AST.method.Method;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * Class declaration AST node.
 */
public class ICClass extends ASTNode {
	private String name;

	private String superClassName = null;

	private List<Field> fields;

	private List<Method> methods;

	public Object accept(Visitor visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}

	/**
	 * Constructs a new class node.
	 * 
	 * @param line
	 *            Line number of class declaration.
	 * @param name
	 *            Class identifier name.
	 * @param fields
	 *            List of all fields in the class.
	 * @param methods
	 *            List of all methods in the class.
	 */
	public ICClass(int line, String name, List<Field> fields,
			List<Method> methods) {
		super(line);
		this.name = name;
		this.fields = fields;
		this.methods = methods;
	}

	public ICClass(int line, String name, List<Method> methods) {
		super(line);
		this.name = name;
		this.fields = new LinkedList<Field>();
		this.methods = methods;
	}

	/**
	 * Constructs a new class node, with a superclass.
	 * 
	 * @param line
	 *            Line number of class declaration.
	 * @param name
	 *            Class identifier name.
	 * @param superClassName
	 *            Superclass identifier name.
	 * @param fields
	 *            List of all fields in the class.
	 * @param methods
	 *            List of all methods in the class.
	 */
	public ICClass(int line, String name, String superClassName,
			List<Field> fields, List<Method> methods) {
		this(line, name, fields, methods);
		this.superClassName = superClassName;
	}

	public String getName() {
		return name;
	}

	public boolean hasSuperClass() {
		return (superClassName != null);
	}

	public String getSuperClassName() {
		return superClassName;
	}

	public List<Field> getFields() {
		return fields;
	}

	public List<Method> getMethods() {
		return methods;
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context)  throws SemanticError{
		return v.visit(this, context);
	}
	
	@Override
	public int hashCode() {
		return name.length();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other==null)
			return false;
		if (!(other instanceof ICClass))
			return false;
		if (other==this)
			return true;
		return name.equals(((ICClass)other).getName());
	}
}
