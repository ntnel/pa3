package IC.AST.type;

import IC.AST.Visitor;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * User-defined data type AST node.
 */
public class UserType extends Type 
{
	private String name;

	public Object accept(Visitor visitor) 
	{
		return visitor.visit(this);
	}
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}
	/**
	 * Constructs a new user-defined data type node.
	 * 
	 * @param line
	 *            Line number of type declaration.
	 * @param name
	 *            Name of data type.
	 */
	public UserType(int line, String name) 
	{
		super(line);
		this.name = name;
	}

	public String getName() 
	{
		return name;
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context)   throws SemanticError{
		return v.visit(this, context);
	}
}
