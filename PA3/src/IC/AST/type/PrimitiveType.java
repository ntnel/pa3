package IC.AST.type;

import IC.DataTypes;
import IC.AST.Visitor;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * Primitive data type AST node.
 */
public class PrimitiveType extends Type 
{
	private DataTypes type;

	public Object accept(Visitor visitor) 
	{
		return visitor.visit(this);
	}
	
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}

	/**
	 * Constructs a new primitive data type node.
	 * 
	 * @param line
	 *            Line number of type declaration.
	 * @param type
	 *            Specific primitive data type.
	 */
	public PrimitiveType(int line, DataTypes type) 
	{
		super(line);
		this.type = type;
	}

	public String getName() 
	{
		return type.getDescription();
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context) throws SemanticError {
		return v.visit(this, context);
	}

	public DataTypes getType() {
		return type;
	}
}