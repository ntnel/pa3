package IC.AST.field;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import IC.AST.method.Method;

public class FieldAndMethodList {
	
	List<Field> fieldsList;
	List<Method> methosdList;
	
	public FieldAndMethodList() 
	{
		methosdList = new LinkedList<Method>();
		fieldsList = new LinkedList<Field>();
		
	}
	
	public void AddMethod(Method method) 
	{
		methosdList.add(0,method);
	}
	
	public void AddFields(List<Field> fields) 
	{
		fieldsList.addAll(0,fields);
	}

	public void AddMethod(List<Method> methods) 
	{
		methosdList.addAll(0,methods);
	}
	
	public List<Method> GetMethods()
	{
		return methosdList;
	}
	
	public List<Field> GetFields()
	{
		return fieldsList;
	}
	
	
	
}
