package IC.AST.method;

import java.util.List;

import IC.AST.Visitor;
import IC.AST.formal.Formal;
import IC.AST.statement.Statement;
import IC.AST.type.Type;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * Static method AST node.
 * 
 * @author Tovi Almozlino
 */
public class StaticMethod extends Method {

	public Object accept(Visitor visitor) {
		return visitor.visit(this);
	}
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}
	/**
	 * Constructs a new static method node.
	 * 
	 * @param type
	 *            Data type returned by method.
	 * @param name
	 *            Name of method.
	 * @param formals
	 *            List of method parameters.
	 * @param statements
	 *            List of method's statements.
	 */
	public StaticMethod(Type type, String name, List<Formal> formals,
			List<Statement> statements) {
		super(type, name, formals, statements);
	}


	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context) throws SemanticError {
		return v.visit(this, context);
	}
}
