package IC.AST.expression;

import IC.AST.Visitor;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * AST node for expression in parentheses.
 */
public class ExpressionBlock extends Expression {

	private Expression expression;

	public Object accept(Visitor visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}

	/**
	 * Constructs a new expression in parentheses node.
	 * 
	 * @param expression
	 *            The expression.
	 */
	public ExpressionBlock(Expression expression) {
		super(expression.getLine());
		this.expression = expression;
	}

	public Expression getExpression() {
		return expression;
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context) throws SemanticError {
		return v.visit(this, context);
	}

}
