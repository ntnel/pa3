package IC.AST.expression;

import IC.AST.Visitor;
import IC.SemanticChecks.ExceptionVisitor;
import IC.SemanticChecks.PropagatingVisitor;
import IC.SemanticChecks.SemanticError;

/**
 * Array length expression AST node.
 */
public class Length extends Expression {

	private Expression array;

	public Object accept(Visitor visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public  void accept(ExceptionVisitor visitor) throws SemanticError {
		 visitor.visit(this);
	}

	/**
	 * Constructs a new array length expression node.
	 * 
	 * @param array
	 *            Expression representing an array.
	 */
	public Length(Expression array) {
		super(array.getLine());
		this.array = array;
	}

	public Expression getArray() {
		return array;
	}
	
	@Override
	public <D, U> U accept(PropagatingVisitor<D, U> v, D context) throws SemanticError {
		return v.visit(this, context);
	}

}
