package IC.SemanticChecks;

import IC.AST.expression.ArrayLocation;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;

/**
 * AST visitor interface. Declares methods for visiting each type of AST node.
 */
public interface PropagatingVisitor<T,S> 
{
	public S visit(Program program,T context) throws SemanticError;

	public S visit(ICClass icClass,T context) throws SemanticError;

	public S visit(Field field,T context) throws SemanticError;

	public S visit(VirtualMethod method,T context) throws SemanticError;

	public S visit(StaticMethod method,T context) throws SemanticError;

	public S visit(LibraryMethod method,T context) throws SemanticError;

	public S visit(Formal formal,T context) throws SemanticError;

	public S visit(PrimitiveType type,T context) throws SemanticError;

	public S visit(UserType type,T context) throws SemanticError;

	public S visit(Assignment assignment,T context) throws SemanticError;

	public S visit(CallStatement callStatement,T context) throws SemanticError;

	public S visit(Return returnStatement,T context) throws SemanticError;

	public S visit(If ifStatement,T context) throws SemanticError;

	public S visit(While whileStatement,T context) throws SemanticError;

	public S visit(Break breakStatement,T context) throws SemanticError;

	public S visit(Continue continueStatement,T context) throws SemanticError;

	public S visit(StatementsBlock statementsBlock,T context) throws SemanticError;

	public S visit(LocalVariable localVariable,T context) throws SemanticError;

	public S visit(VariableLocation location,T context) throws SemanticError;

	public S visit(ArrayLocation location,T context) throws SemanticError;

	public S visit(StaticCall call,T context) throws SemanticError;

	public S visit(VirtualCall call,T context) throws SemanticError;

	public S visit(This thisExpression,T context) throws SemanticError;

	public S visit(NewClass newClass,T context) throws SemanticError;

	public S visit(NewArray newArray,T context) throws SemanticError;

	public S visit(Length length,T context) throws SemanticError;

	public S visit(MathBinaryOp binaryOp,T context) throws SemanticError;

	public S visit(LogicalBinaryOp binaryOp,T context) throws SemanticError;

	public S visit(MathUnaryOp unaryOp,T context) throws SemanticError;

	public S visit(LogicalUnaryOp unaryOp,T context) throws SemanticError;

	public S visit(Literal literal,T context) throws SemanticError;

	public S visit(ExpressionBlock expressionBlock,T context) throws SemanticError;
}
