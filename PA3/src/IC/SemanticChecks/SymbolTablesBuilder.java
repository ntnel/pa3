package IC.SemanticChecks;

import java.util.LinkedList;

import IC.AST.expression.ArrayLocation;
import IC.AST.expression.Expression;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.Method;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.Statement;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;
import IC.SymbolTable.ClassSymbolTable;
import IC.SymbolTable.Kind;
import IC.SymbolTable.Symbol;
import IC.SymbolTable.SymbolTable;
import IC.SymbolTable.SymbolTable.tableTypes;
import IC.Types.MethodType;
import IC.Types.Type;
import IC.Types.TypeTable;

public class SymbolTablesBuilder implements
		PropagatingVisitor<SymbolTable, Type> {

	private Program root;
	private SymbolTable globalTable;
	private boolean seenMainMethod;

	public SymbolTable build(Program root) throws SemanticError {
		this.root = root;
		root.accept(this, null);

		if (!seenMainMethod)
			ErrorManager.throwMainMethodError();

		return globalTable;
	}

	@Override
	public Type visit(Program program, SymbolTable parentTable)
			throws SemanticError {
		globalTable = new SymbolTable("global", tableTypes.GLOBAL);
		for (ICClass pClass : program.getClasses())
			pClass.accept(this, globalTable);
		return null;
	}

	@Override
	public Type visit(ICClass icClass, SymbolTable parentTable)
			throws SemanticError {
		// Verify class doesn't extend itself
		if (icClass.getName().equals(icClass.getSuperClassName()))
			ErrorManager.throwSelfExtendError(icClass);

		// Verify that if a class extends a class, it was already defined.
		// Thus making sure the class hierarchy is acyclic
		if (icClass.hasSuperClass()
				&& parentTable.getChildTableById(icClass.getSuperClassName()) == null)
			ErrorManager.throwUnidentifiedTypeError(icClass.getLine(),
					icClass.getSuperClassName());

		// Create a symbol for the class, assign it a type (via TypeTable) and
		// add it to the global table
		Symbol classSymbol = new Symbol(icClass.getName(), Kind.CLASS);
		classSymbol.setType(TypeTable.getInstance().getClassType(icClass));
		parentTable.insert(classSymbol);

		// Create a symbol table for the class
		ClassSymbolTable classTable = new ClassSymbolTable(icClass.getName());

		// Add this class table as a child of the global table
		parentTable.addChildSymbolTable(classTable);

		// If class has super class, nest the class table in its super class
		// table
		if (icClass.hasSuperClass()) {
			SymbolTable superClassTable = parentTable.getChildTableById(icClass
					.getSuperClassName());
			superClassTable.addChildSymbolTable(classTable);
			icClass.setEnclosingScope(superClassTable);
		} else
			icClass.setEnclosingScope(parentTable);

		// Proceed to visit the class node children (fields and methods)
		for (Field field : icClass.getFields())
			field.accept(this, classTable);
		for (Method method : icClass.getMethods()) {
			SymbolTable scope = (method instanceof StaticMethod || method instanceof LibraryMethod) ? classTable
					.getStaticScope() : classTable;
			method.accept(this, scope);
		}
		return null;

	}

	@Override
	public Type visit(Field field, SymbolTable parentTable)
			throws SemanticError {
		if (parentTable.lookUp(field.getName()) != null)
			ErrorManager.throwDuplicateIDError(field.getLine(), field.getName(),
					parentTable.getId());

		Symbol fieldSymbol = new Symbol(field.getName(), Kind.FIELD);
		parentTable.insert(fieldSymbol);
		Type fieldType = field.getType().accept(this, parentTable);
		fieldSymbol.setType(fieldType);

		field.setEnclosingScope(parentTable);
		return null;
	}

	private void checkMethodOverriding(Method method, Symbol methodSymbol,
			SymbolTable parentTable) throws SemanticError {

		// Look in upper scopes. If found, check if the other symbol is a method
		// too
		Symbol upperSymbol = parentTable.getParentSymbolTable().lookUp(
				method.getName());

		// If found and not same kind throw duplicate id error
		if (upperSymbol != null) {
			if (upperSymbol.getKind() != methodSymbol.getKind())
				ErrorManager.throwDuplicateIDError(method.getLine(), method.getName(),
						parentTable.getId());
			// Found matching method name and kind in upper scopes, check overriding
			else {
				boolean methodsSignatureMatch = ((MethodType) methodSymbol
						.getType()).matches((MethodType) upperSymbol.getType());
				if (!methodsSignatureMatch) {
					// Found incorrect overriding(same name, different
					// signature). Throw error
					ErrorManager.throwWrongOverridingError(method, methodSymbol,
							upperSymbol, parentTable.getId());
				}
			}
		}
	}

	private Type visitMethod(Method method, int methodType,
			SymbolTable parentTable) throws SemanticError {

		// Look for the same identifier, only in the class the method was
		// declared in. If same identifier found in same class, throw error
		if (parentTable.getSymbol(method.getName()) != null)
			ErrorManager.throwDuplicateIDError(method.getLine(), method.getName(), parentTable.getId());

		SymbolTable methodTable = new SymbolTable(method.getName(),
				tableTypes.METHOD);
		parentTable.addChildSymbolTable(methodTable);

		if (methodType == 0)
			methodTable.insert(new Symbol("this", null));

		LinkedList<Type> formalsSymbolsTypes = new LinkedList<Type>();
		for (Formal formal : method.getFormals()) {
			Type formalSymbolType = formal.accept(this, methodTable);
			formalsSymbolsTypes.add(formalSymbolType);
		}

		if (methodType != 2) {
			for (Statement stmt : method.getStatements())
				stmt.accept(this, methodTable);
		}

		Symbol methodSymbol = new Symbol(method.getName(),
				methodType == 0 ? Kind.METHOD_VIRTUAL : Kind.METHOD_STATIC);
		parentTable.insert(methodSymbol);

		Symbol returnSymbol = new Symbol("$ret", null);
		Type returnSymbolType = method.getType().accept(this, parentTable);
		returnSymbol.setType(returnSymbolType);
		methodTable.insert(returnSymbol);

		MethodType mType = TypeTable.getInstance().getMethodType(
				formalsSymbolsTypes, returnSymbolType);
		methodSymbol.setType(mType);

		// Look for method in upper scopes and verify correct overriding
		checkMethodOverriding(method, methodSymbol, parentTable);

		boolean isMainMethod = method.getName().equals("main")
				&& TypeTable.isMainMethodTypeStructure(mType)
				&& methodType == 1;
		if (isMainMethod) {
			if (seenMainMethod)
				ErrorManager.throwMainMethodError();
			else
				seenMainMethod = true;
		}

		method.setEnclosingScope(parentTable);
		return null;

	}

	@Override
	public Type visit(VirtualMethod method, SymbolTable parentTable)
			throws SemanticError {
		return visitMethod(method, 0, parentTable);

	}

	@Override
	public Type visit(StaticMethod method, SymbolTable parentTable)
			throws SemanticError {
		return visitMethod(method, 1, parentTable);
	}

	@Override
	public Type visit(LibraryMethod method, SymbolTable parentTable)
			throws SemanticError {
		return visitMethod(method, 2, parentTable);
	}

	@Override
	public Type visit(Formal formal, SymbolTable parentTable)
			throws SemanticError {
		Symbol formalSymbol = new Symbol(formal.getName(), Kind.FORMAL);
		Type formalSymbolType = formal.getType().accept(this, parentTable);
		formalSymbol.setType(formalSymbolType);
		parentTable.insert(formalSymbol);
		formal.setEnclosingScope(parentTable);
		return formalSymbolType;
	}

	@Override
	public Type visit(PrimitiveType type, SymbolTable parentTable)
			throws SemanticError {
		type.setEnclosingScope(parentTable);

		TypeTable globalTypeTable = TypeTable.getInstance();
		int typeDimension = type.getDimension();
		Type lastType = globalTypeTable.getPrimType(type.getType()
				.getDescription());
		for (int i = 0; i < typeDimension; i++) {
			lastType = globalTypeTable.getArrayType(lastType);
		}
		return lastType;
	}

	@Override
	public Type visit(UserType type, SymbolTable parentTable)
			throws SemanticError {

		TypeTable globalTypeTable = TypeTable.getInstance();
		int typeDimension = type.getDimension();
		ICClass typeClass = root.getClass(type.getName());
		if (typeClass == null)
			ErrorManager.throwUnidentifiedTypeError(type.getLine(), type.getName());

		Type lastType = globalTypeTable.getClassType(typeClass);
		for (int i = 0; i < typeDimension; i++) {
			lastType = globalTypeTable.getArrayType(lastType);
		}
		type.setEnclosingScope(parentTable);
		return lastType;
	}

	@Override
	public Type visit(Assignment assignment, SymbolTable parentTable)
			throws SemanticError {
		assignment.getVariable().accept(this, parentTable);
		assignment.getAssignment().accept(this, parentTable);
		assignment.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(CallStatement callStatement, SymbolTable parentTable)
			throws SemanticError {
		callStatement.getCall().accept(this, parentTable);
		callStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(Return returnStatement, SymbolTable parentTable)
			throws SemanticError {
		if (returnStatement.hasValue())
			returnStatement.getValue().accept(this, parentTable);
		returnStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(If ifStatement, SymbolTable parentTable)
			throws SemanticError {
		ifStatement.getCondition().accept(this, parentTable);
		ifStatement.getOperation().accept(this, parentTable);
		if (ifStatement.hasElse())
			ifStatement.getElseOperation().accept(this, parentTable);

		ifStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(While whileStatement, SymbolTable parentTable)
			throws SemanticError {
		whileStatement.getCondition().accept(this, parentTable);
		whileStatement.getOperation().accept(this, parentTable);
		whileStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(Break breakStatement, SymbolTable parentTable)
			throws SemanticError {
		breakStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(Continue continueStatement, SymbolTable parentTable)
			throws SemanticError {
		continueStatement.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(StatementsBlock statementsBlock, SymbolTable parentTable)
			throws SemanticError {
		parentTable.insert(new Symbol("statement block", Kind.STATMENT_BLOCK));
		SymbolTable blockTable = new SymbolTable("statement block in "
				+ parentTable.getId(), tableTypes.BLOCK);
		parentTable.addChildSymbolTable(blockTable);
		for (Statement stmt : statementsBlock.getStatements())
			stmt.accept(this, blockTable);

		statementsBlock.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(LocalVariable localVariable, SymbolTable parentTable)
			throws SemanticError {
		if (parentTable.contains(localVariable.getName()))
			ErrorManager.throwDuplicateIDError(localVariable.getLine(),localVariable.getName(), parentTable.getId());

		if (localVariable.hasInitValue())
			localVariable.getInitValue().accept(this, parentTable);

		Symbol varSymbol = new Symbol(localVariable.getName(), Kind.VARIABLE);
		Type varSymbolType = localVariable.getType().accept(this, parentTable);
		varSymbol.setType(varSymbolType);
		parentTable.insert(varSymbol);

		localVariable.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(VariableLocation location, SymbolTable parentTable)
			throws SemanticError {
		if (location.isExternal())
			location.getLocation().accept(this, parentTable);
		location.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(ArrayLocation location, SymbolTable parentTable)
			throws SemanticError {
		location.getArray().accept(this, parentTable);
		location.getIndex().accept(this, parentTable);
		location.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(StaticCall call, SymbolTable parentTable)
			throws SemanticError {
		for (Expression argument : call.getArguments())
			argument.accept(this, parentTable);
		call.setEnclosingScope(parentTable);
		return null;

	}

	@Override
	public Type visit(VirtualCall call, SymbolTable parentTable)
			throws SemanticError {
		if (call.isExternal())
			call.getLocation().accept(this, parentTable);
		for (Expression argument : call.getArguments())
			argument.accept(this, parentTable);
		call.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(This thisExpression, SymbolTable parentTable)
			throws SemanticError {
		thisExpression.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(NewClass newClass, SymbolTable parentTable)
			throws SemanticError {
		newClass.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(NewArray newArray, SymbolTable parentTable)
			throws SemanticError {
		newArray.getType().accept(this, parentTable);
		newArray.getSize().accept(this, parentTable);
		newArray.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(Length length, SymbolTable parentTable)
			throws SemanticError {
		length.getArray().accept(this, parentTable);
		length.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(MathBinaryOp binaryOp, SymbolTable parentTable)
			throws SemanticError {
		binaryOp.getFirstOperand().accept(this, parentTable);
		binaryOp.getSecondOperand().accept(this, parentTable);
		binaryOp.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(LogicalBinaryOp binaryOp, SymbolTable parentTable)
			throws SemanticError {
		binaryOp.getFirstOperand().accept(this, parentTable);
		binaryOp.getSecondOperand().accept(this, parentTable);
		binaryOp.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(MathUnaryOp unaryOp, SymbolTable parentTable)
			throws SemanticError {
		unaryOp.getOperand().accept(this, parentTable);
		unaryOp.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(LogicalUnaryOp unaryOp, SymbolTable parentTable)
			throws SemanticError {
		unaryOp.getOperand().accept(this, parentTable);
		unaryOp.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(Literal literal, SymbolTable parentTable)
			throws SemanticError {
		literal.setEnclosingScope(parentTable);
		return null;
	}

	@Override
	public Type visit(ExpressionBlock expressionBlock, SymbolTable parentTable)
			throws SemanticError {
		expressionBlock.getExpression().accept(this, parentTable);
		expressionBlock.setEnclosingScope(parentTable);
		return null;
	}
}
