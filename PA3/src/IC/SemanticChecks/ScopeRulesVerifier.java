package IC.SemanticChecks;

import IC.AST.expression.ArrayLocation;
import IC.AST.expression.Expression;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.Method;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.Statement;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;
import IC.SymbolTable.ClassSymbolTable;
import IC.SymbolTable.SymbolTable;

public class ScopeRulesVerifier implements ExceptionVisitor {

	private SymbolTable globalScope;
	private int loopCount;

	public void verify(Program root, SymbolTable globalScope) throws Exception {
		this.globalScope = globalScope;
		root.accept(this);
	}

	@Override
	public void visit(Program program) throws SemanticError {
		for (ICClass pClass : program.getClasses())
			pClass.accept(this);

	}

	@Override
	public void visit(ICClass icClass) throws SemanticError {
		for (Field field : icClass.getFields())
			field.accept(this);
		for (Method method : icClass.getMethods())
			method.accept(this);
	}

	@Override
	public void visit(Field field) throws SemanticError {
		field.getType().accept(this);
	}

	private void visitMethod(Method method) throws SemanticError {
		method.getType().accept(this);
		for (Formal formal : method.getFormals())
			formal.accept(this);
		for (Statement stmt : method.getStatements())
			stmt.accept(this);
	}

	@Override
	public void visit(VirtualMethod method) throws SemanticError {
		visitMethod(method);

	}

	@Override
	public void visit(StaticMethod method) throws SemanticError {
		visitMethod(method);

	}

	@Override
	public void visit(LibraryMethod method) throws SemanticError {
		visitMethod(method);
	}

	@Override
	public void visit(Formal formal) throws SemanticError {
		formal.getType().accept(this);
	}

	@Override
	public void visit(PrimitiveType type) throws SemanticError {
	}

	@Override
	public void visit(UserType type) throws SemanticError {
		if (type.enclosingScope().lookUp(type.getName()) == null)
			ErrorManager.throwUnidentifiedTypeError(type.getLine(),
					type.getName());
	}

	@Override
	public void visit(Assignment assignment) throws SemanticError {
		assignment.getAssignment().accept(this);
		assignment.getVariable().accept(this);
	}

	@Override
	public void visit(CallStatement callStatement) throws SemanticError {
		callStatement.getCall().accept(this);
	}

	@Override
	public void visit(Return returnStatement) throws SemanticError {
		if (returnStatement.hasValue())
			returnStatement.getValue().accept(this);

	}

	@Override
	public void visit(If ifStatement) throws SemanticError {
		ifStatement.getCondition().accept(this);
		ifStatement.getOperation().accept(this);
		if (ifStatement.hasElse())
			ifStatement.getElseOperation().accept(this);

	}

	@Override
	public void visit(While whileStatement) throws SemanticError {
		whileStatement.getCondition().accept(this);
		loopCount++;
		whileStatement.getOperation().accept(this);
		loopCount--;

	}

	@Override
	public void visit(Break breakStatement) throws SemanticError {
		boolean insideLoop = loopCount != 0;
		if (!insideLoop)
			ErrorManager.throwLoopKeywordOutOfLoopError("break",
					breakStatement.getLine());
	}

	@Override
	public void visit(Continue continueStatement) throws SemanticError {
		boolean insideLoop = loopCount != 0;
		if (!insideLoop)
			ErrorManager.throwLoopKeywordOutOfLoopError("continue",
					continueStatement.getLine());
	}

	@Override
	public void visit(StatementsBlock statementsBlock) throws SemanticError {
		for (Statement stmt : statementsBlock.getStatements())
			stmt.accept(this);
	}

	@Override
	public void visit(LocalVariable localVariable) throws SemanticError {
		localVariable.getType().accept(this);
		if (localVariable.hasInitValue())
			localVariable.getInitValue().accept(this);
	}

	@Override
	public void visit(VariableLocation location) throws SemanticError {
		if (location.isExternal())
			location.getLocation().accept(this);
		else if (location.enclosingScope().lookUp(location.getName()) == null)
			ErrorManager.throwUnresolvedVariable(location.getLine(),
					location.getName());
	}

	@Override
	public void visit(ArrayLocation location) throws SemanticError {
		location.getArray().accept(this);
		location.getIndex().accept(this);
	}

	@Override
	public void visit(StaticCall call) throws SemanticError {
		ClassSymbolTable methodClassScope = (ClassSymbolTable) globalScope
				.getChildTableById(call.getClassName());
		if (methodClassScope == null)
			ErrorManager.throwUnidentifiedTypeError(call.getLine(),
					call.getClassName());
		if (methodClassScope.getStaticScope().lookUp(call.getName()) == null)
			ErrorManager
					.throwUndefinedMethodError(call.getLine(), call.getName(),
							"static", call.getClassEnclosingScope().getId());
		for (Expression argument : call.getArguments())
			argument.accept(this);
	}

	// Context is the enclosing class symbol table
	@Override
	public void visit(VirtualCall call) throws SemanticError {
		SymbolTable scope = call.enclosingScope();
		if (call.isExternal())
			call.getLocation().accept(this);
		else {
			if (scope.lookUp("this") == null)
				ErrorManager.throwVirtualCallFromStaticContext(call.getLine());

			if (scope.lookUp(call.getName()) == null)
				ErrorManager.throwUndefinedMethodError(call.getLine(), call
						.getName(), "virtual", call.getClassEnclosingScope()
						.getId());
		}
		for (Expression argument : call.getArguments())
			argument.accept(this);
	}

	@Override
	public void visit(This thisExpression) throws SemanticError {
		if (thisExpression.enclosingScope().lookUp("this") == null)
			ErrorManager.throwUseOfThisInStaticContextError(thisExpression
					.getLine());
	}

	@Override
	public void visit(NewClass newClass) throws SemanticError {
		if (newClass.enclosingScope().lookUp(newClass.getName()) == null)
			ErrorManager.throwUnidentifiedTypeError(newClass.getLine(),
					newClass.getName());
	}

	@Override
	public void visit(NewArray newArray) throws SemanticError {
		newArray.getType().accept(this);
		newArray.getSize().accept(this);
	}

	@Override
	public void visit(Length length) throws SemanticError {
		length.getArray().accept(this);
	}

	@Override
	public void visit(MathBinaryOp binaryOp) throws SemanticError {
		binaryOp.getFirstOperand().accept(this);
		binaryOp.getSecondOperand().accept(this);

	}

	@Override
	public void visit(LogicalBinaryOp binaryOp) throws SemanticError {
		binaryOp.getFirstOperand().accept(this);
		binaryOp.getSecondOperand().accept(this);
	}

	@Override
	public void visit(MathUnaryOp unaryOp) throws SemanticError {
		unaryOp.getOperand().accept(this);

	}

	@Override
	public void visit(LogicalUnaryOp unaryOp) throws SemanticError {
		unaryOp.getOperand().accept(this);

	}

	@Override
	public void visit(Literal literal) throws SemanticError {
		// Nothing to do

	}

	@Override
	public void visit(ExpressionBlock expressionBlock) throws SemanticError {
		expressionBlock.getExpression().accept(this);
	}

}
