package IC.SemanticChecks.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import IC.Compiler;

public class SemanticChecksTester {
	
	public static void ExecuteTestsMain()
	{
		List<File> testFiles = null;

		String dir = System.getProperty("user.dir");
		String currentDir =  File.separator + "src" + File.separator + "IC" + File.separator + "SemanticChecks" + File.separator + "Test";	
		dir += currentDir;
		
		testFiles = enumerateFile(dir);
		System.out.println("Total amount of files to test = " + testFiles.size());
		System.out.println("---------------------------------------------------------------");
		
		RunTests(testFiles);
	}
	
	// Get the test files and run the compiler on each
	private static void RunTests(List<File> files)
	{
		BufferedReader br = null;

		for (File file : files) 
		{
			System.out.println(file.getPath());
			try 
			{
				// get the first row in the file that describe the test and print it
				br = new BufferedReader(new FileReader(file.getPath()));
				String comment = br.readLine();
				System.out.println(comment.substring(3, comment.length()));
				
				// Run the compiler of the .ic file
				Compiler.doRun(new String[]{ file.getPath() });
				System.out.println("---------------------------------------------------------------");
			} 
			catch (FileNotFoundException e) 
			{
				e.printStackTrace();
			} 
			catch (IOException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	// Return enumeration of all the test files in the sub-directories
	private static List<File> enumerateFile(String myDirectoryPath)
	{
		List<File> files = new LinkedList<File>();

		File dir = new File(myDirectoryPath);
		File[] directories = dir.listFiles();
		
		for (File directory : directories)
		{
			if (directory.isDirectory())
			{
	            System.out.println("Directory: " + directory.getName());
	            files.addAll(Arrays.asList(directory.listFiles()));
			}
		}
		
		return files;
	}
}
