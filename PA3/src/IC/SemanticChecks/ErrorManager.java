package IC.SemanticChecks;

import IC.AST.icclass.ICClass;
import IC.AST.method.Method;
import IC.SymbolTable.Symbol;

public class ErrorManager {
	
	private static void throwError(int line, String msg) throws SemanticError {
		throw new SemanticError(line, msg);

	}

	protected static void throwUnidentifiedTypeError(int line, String typeName)
			throws SemanticError {
		String errMsg = String.format("Reference to unidentified type '%s'",
				typeName);
		throwError(line, errMsg);
	}

	protected static void throwSelfExtendError(ICClass badClass) throws SemanticError {
		String errMsg = String.format("Type %s cannot extend itself",
				badClass.getName());
		throwError(badClass.getLine(), errMsg);
	}

	protected static void throwMainMethodError() throws SemanticError {
		String errMsg = "Program must define exactly one main method with the following signature: static void main(string[])";
		throwError(0, errMsg);
	}

	protected static void throwDuplicateIDError(int line, String dupId, String where)
			throws SemanticError {
		String errMsg = String.format("Duplicate identifier! '%s.%s'", where,
				dupId);
		throwError(line, errMsg);
	}

	protected static void throwWrongOverridingError(Method method,
			Symbol methodSymbol, Symbol upperMethodSymbol, String where)
			throws SemanticError {
		String errMsg = String.format(
				"%s %s in type %s must match overriden method signature: %s",
				methodSymbol.getKind().getDescription(), method.getName(),
				where, upperMethodSymbol.getType().getDescription());
		throwError(method.getLine(), errMsg);
	}
	
	protected static void throwLoopKeywordOutOfLoopError(String keyword,int line) throws SemanticError{
		throwError(line, keyword+" cannot be used outside of a loop");
	}
	
	protected static void throwUnresolvedVariable( int line,String name) throws SemanticError{
		String errMsg=String.format("'%s' cannot be resolved to a variable",
				name);
		throwError(line, errMsg);
	}
	
	protected static void throwUseOfThisInStaticContextError(int line) throws SemanticError{
		throwError(line, "'this' cannot be used in a static context");
	}
	
	protected static void throwUndefinedMethodError(int line, String methodName,
			String methodKind, String where) throws SemanticError {
		String errMsg= String.format(
				"The %s method '%s' is undefined for the type %s",methodKind,
				methodName, where);
		throwError(line, errMsg);
	}
	
	protected static void throwVirtualCallFromStaticContext(int line) throws SemanticError{
		throwError(line, "Cannot make a virtual call from a static context");
	}

}
