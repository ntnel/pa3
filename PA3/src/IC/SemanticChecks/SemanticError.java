package IC.SemanticChecks;

public class SemanticError extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4380991492950438958L;
	public String msg;

	public SemanticError(int errLine, String errMsg) {
		this.msg = "semantic error at line " + errLine + ": " + errMsg;
	}


}
