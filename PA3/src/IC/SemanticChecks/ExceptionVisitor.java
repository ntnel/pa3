package IC.SemanticChecks;

import IC.AST.expression.ArrayLocation;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;

/**
 * AST visitor interface. Declares methods for visiting each type of AST node.
 */
public interface ExceptionVisitor 
{
	public void visit(Program program) throws SemanticError;

	public void visit(ICClass icClass) throws SemanticError;

	public void visit(Field field) throws SemanticError;

	public void visit(VirtualMethod method) throws SemanticError;

	public void visit(StaticMethod method) throws SemanticError;

	public void visit(LibraryMethod method) throws SemanticError;

	public void visit(Formal formal) throws SemanticError;

	public void visit(PrimitiveType type) throws SemanticError;

	public void visit(UserType type) throws SemanticError;

	public void visit(Assignment assignment) throws SemanticError;

	public void visit(CallStatement callStatement) throws SemanticError;

	public void visit(Return returnStatement) throws SemanticError;

	public void visit(If ifStatement) throws SemanticError;

	public void visit(While whileStatement) throws SemanticError;

	public void visit(Break breakStatement) throws SemanticError;

	public void visit(Continue continueStatement) throws SemanticError;

	public void visit(StatementsBlock statementsBlock) throws SemanticError;

	public void visit(LocalVariable localVariable) throws SemanticError;

	public void visit(VariableLocation location) throws SemanticError;

	public void visit(ArrayLocation location) throws SemanticError;

	public void visit(StaticCall call) throws SemanticError;

	public void visit(VirtualCall call) throws SemanticError;

	public void visit(This thisExpression) throws SemanticError;

	public void visit(NewClass newClass) throws SemanticError;

	public void visit(NewArray newArray) throws SemanticError;

	public void visit(Length length) throws SemanticError;

	public void visit(MathBinaryOp binaryOp) throws SemanticError;

	public void visit(LogicalBinaryOp binaryOp) throws SemanticError;

	public void visit(MathUnaryOp unaryOp) throws SemanticError;

	public void visit(LogicalUnaryOp unaryOp) throws SemanticError;

	public void visit(Literal literal) throws SemanticError;

	public void visit(ExpressionBlock expressionBlock) throws SemanticError;
}
