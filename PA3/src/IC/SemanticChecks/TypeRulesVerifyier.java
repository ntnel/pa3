package IC.SemanticChecks;

import java.util.List;

import IC.BinaryOps;
import IC.AST.expression.ArrayLocation;
import IC.AST.expression.Call;
import IC.AST.expression.Expression;
import IC.AST.expression.ExpressionBlock;
import IC.AST.expression.Length;
import IC.AST.expression.Literal;
import IC.AST.expression.LogicalBinaryOp;
import IC.AST.expression.LogicalUnaryOp;
import IC.AST.expression.MathBinaryOp;
import IC.AST.expression.MathUnaryOp;
import IC.AST.expression.NewArray;
import IC.AST.expression.NewClass;
import IC.AST.expression.StaticCall;
import IC.AST.expression.This;
import IC.AST.expression.VariableLocation;
import IC.AST.expression.VirtualCall;
import IC.AST.field.Field;
import IC.AST.formal.Formal;
import IC.AST.icclass.ICClass;
import IC.AST.method.LibraryMethod;
import IC.AST.method.Method;
import IC.AST.method.StaticMethod;
import IC.AST.method.VirtualMethod;
import IC.AST.program.Program;
import IC.AST.statement.Assignment;
import IC.AST.statement.Break;
import IC.AST.statement.CallStatement;
import IC.AST.statement.Continue;
import IC.AST.statement.If;
import IC.AST.statement.LocalVariable;
import IC.AST.statement.Return;
import IC.AST.statement.Statement;
import IC.AST.statement.StatementsBlock;
import IC.AST.statement.While;
import IC.AST.type.PrimitiveType;
import IC.AST.type.UserType;
import IC.SymbolTable.Kind;
import IC.SymbolTable.Symbol;
import IC.SymbolTable.SymbolTable;
import IC.Types.ArrayType;
import IC.Types.ClassType;
import IC.Types.MethodType;
import IC.Types.PrimType;
import IC.Types.PrimitiveTypes;
import IC.Types.Type;
import IC.Types.TypeTable;

public class TypeRulesVerifyier implements ExceptionVisitor {

	TypeTable globalTypeTable;
	SymbolTable globalSymbolTable;
	Program root;

	public void verify(Program root, SymbolTable globalSymbolTable)
			throws SemanticError {
		globalTypeTable = TypeTable.getInstance();
		this.globalSymbolTable = globalSymbolTable;
		this.root = root;
		root.accept(this);
	}

	@Override
	public void visit(Program program) throws SemanticError {
		for (ICClass icClass : program.getClasses())
			icClass.accept(this);

	}

	@Override
	public void visit(ICClass icClass) throws SemanticError {
		for (Method method : icClass.getMethods())
			method.accept(this);

	}

	@Override
	public void visit(Field field) throws SemanticError {

	}

	private void visitMethod(Method method, int type) throws SemanticError {
		method.getType().accept(this);
		for (Formal formal : method.getFormals())
			formal.accept(this);
		for (Statement stmt : method.getStatements())
			stmt.accept(this);
		method.setNodeType(method.enclosingScope().lookUp(method.getName())
				.getType());
	}

	@Override
	public void visit(VirtualMethod method) throws SemanticError {
		visitMethod(method, 0);

	}

	@Override
	public void visit(StaticMethod method) throws SemanticError {
		visitMethod(method, 1);

	}

	@Override
	public void visit(LibraryMethod method) throws SemanticError {
		visitMethod(method, 2);

	}

	@Override
	public void visit(Formal formal) throws SemanticError {
		formal.getType().accept(this);
	}

	@Override
	public void visit(PrimitiveType type) throws SemanticError {
		Type returnType = globalTypeTable.getPrimType(type.getName());
		for (int i = 0; i < type.getDimension(); i++) {
			returnType = globalTypeTable.getArrayType(returnType);
		}
		type.setNodeType(returnType);
	}

	@Override
	public void visit(UserType type) throws SemanticError {
		Type returnType = globalTypeTable.getClassType(root.getClass(type
				.getName()));
		for (int i = 0; i < type.getDimension(); i++) {
			returnType = globalTypeTable.getArrayType(returnType);
		}
		type.setNodeType(returnType);
	}

	@Override
	public void visit(Assignment assignment) throws SemanticError {
		assignment.getAssignment().accept(this);
		assignment.getVariable().accept(this);

		Type varType = assignment.getVariable().getNodeType();
		Type assignType = assignment.getAssignment().getNodeType();

		if (!assignType.isSubtypeOf(varType))
			throw new SemanticError(assignment.getLine(), String.format(
					"Illegal assignment, assignment value must be of type %s",
					assignment.getVariable().getNodeType().getDescription()));
	}

	@Override
	public void visit(CallStatement callStatement) throws SemanticError {
		callStatement.getCall().accept(this);

	}

	@Override
	public void visit(Return returnStatement) throws SemanticError {
		if (returnStatement.hasValue())
			returnStatement.getValue().accept(this);
		Type methodExpectedReturnType = returnStatement.enclosingScope()
				.lookUp("$ret").getType();
		Type returnType = returnStatement.hasValue() ? returnStatement
				.getValue().getNodeType() : globalTypeTable.getPrimType("void");
		if (!returnType.isSubtypeOf(methodExpectedReturnType)) {
			String errMsg = methodExpectedReturnType.getDescription().equals(
					"void") ? "Void methods cannot return a value" : String
					.format("Method must return a result of type %s",
							methodExpectedReturnType.getDescription());
			throw new SemanticError(returnStatement.getLine(), errMsg);
		}

	}

	private void verifyConditionType(String title, int line, Type condType)
			throws SemanticError {
		if (!(condType instanceof PrimType && ((PrimType) condType)
				.getPrimitiveType() == PrimitiveTypes.BOOLEAN))
			throw new SemanticError(line, title
					+ " condition must be of type boolean");
	}

	@Override
	public void visit(If ifStatement) throws SemanticError {
		ifStatement.getCondition().accept(this);
		Type condType = ifStatement.getCondition().getNodeType();
		verifyConditionType("if", ifStatement.getCondition().getLine(),
				condType);

		ifStatement.getOperation().accept(this);
		if (ifStatement.hasElse())
			ifStatement.getElseOperation().accept(this);

	}

	@Override
	public void visit(While whileStatement) throws SemanticError {
		whileStatement.getCondition().accept(this);
		Type condType = whileStatement.getCondition().getNodeType();
		verifyConditionType("while", whileStatement.getCondition().getLine(),
				condType);
	}

	@Override
	public void visit(Break breakStatement) throws SemanticError {
		// Nothing to do

	}

	@Override
	public void visit(Continue continueStatement) throws SemanticError {
		// Nothing to do
	}

	@Override
	public void visit(StatementsBlock statementsBlock) throws SemanticError {
		for (Statement stmt : statementsBlock.getStatements())
			stmt.accept(this);
	}

	@Override
	public void visit(LocalVariable localVariable) throws SemanticError {
		if (localVariable.hasInitValue()) {
			localVariable.getInitValue().accept(this);
			Type varType = localVariable.enclosingScope()
					.lookUp(localVariable.getName()).getType();
			Type initValueType = localVariable.getInitValue().getNodeType();
			if (!initValueType.isSubtypeOf(varType))
				throw new SemanticError(
						localVariable.getLine(),
						String.format(
								"Illegal assignment, assignment value must be of type %s",
								varType.getDescription()));

		}
	}

	@Override
	public void visit(VariableLocation location) throws SemanticError {
		if (location.isExternal()) {
			location.getLocation().accept(this);
			Type externalType = location.getLocation().getNodeType();
			if (externalType instanceof ClassType) {
				SymbolTable externalScope = globalSymbolTable
						.getChildTableById(externalType.getDescription());
				Symbol externalSymbol = externalScope
						.lookUp(location.getName());
				if (externalSymbol == null || externalSymbol.getKind() !=  Kind.FIELD)
					throw new SemanticError(location.getLine(), String.format(
							"Reference to undefined field '%s' in type %s",
							location.getName(), externalType.getDescription()));
				else
					location.setNodeType(externalSymbol.getType());
			} else
				throw new SemanticError(location.getLine(), String.format(
						"Field %s is undefined for type %s",
						location.getName(), location.getLocation()
								.getNodeType().getDescription()));
		} else
			location.setNodeType(location.enclosingScope()
					.lookUp(location.getName()).getType());

	}

	@Override
	public void visit(ArrayLocation location) throws SemanticError {
		location.getArray().accept(this);
		location.getIndex().accept(this);

		Type arrType = location.getArray().getNodeType();
		Type indexType = location.getIndex().getNodeType();

		if (!(arrType instanceof ArrayType))
			throw new SemanticError(location.getLine(),
					"array index query can only be used on an array type");

		if (!isIntType(indexType))
			throw new SemanticError(location.getLine(),
					"array index must be of type int");

		location.setNodeType(((ArrayType) arrType).getCellType());

	}

	private void throwInvalidCallError(Call call, String type, String className)
			throws SemanticError {
		String errMsg = String
				.format("The %s method '%s' in type %s is not applicable for the arguments (",
						type, call.getName(), className);
		for (Expression e : call.getArguments()) {
			errMsg += e.getNodeType().getDescription() + ", ";
		}
		errMsg += ")";
		errMsg = errMsg.replace(", )", ")");
		throw new SemanticError(call.getLine(), errMsg);
	}

	private void visitCall(Call call, String type, String calledClassName)
			throws SemanticError {
		for (Expression arg : call.getArguments())
			arg.accept(this);

		Symbol calledMethodSymbol = globalSymbolTable.getChildTableById(
				calledClassName).lookUp(call.getName());
		Kind expectedKind = type.equals("static") ? Kind.METHOD_STATIC
				: Kind.METHOD_VIRTUAL;
		if (calledMethodSymbol == null
				|| !(calledMethodSymbol.getKind() == expectedKind))
			throw new SemanticError(call.getLine(), String.format(
					"The %s method '%s' is undefined in type %s", type,
					call.getName(), calledClassName));

		MethodType calledMethodType = (MethodType) calledMethodSymbol.getType();
		List<Type> expectedTypes = calledMethodType.getParamTypes();
		List<Expression> argsSent = call.getArguments();

		if (expectedTypes.size() != argsSent.size())
			throwInvalidCallError(call, type, calledClassName);

		for (int i = 0; i < argsSent.size(); i++) {
			if (!argsSent.get(i).getNodeType()
					.isSubtypeOf(expectedTypes.get(i)))
				throwInvalidCallError(call, type, calledClassName);
		}

		call.setNodeType(calledMethodType.getReturnType());
	}

	@Override
	public void visit(StaticCall call) throws SemanticError {
		visitCall(call, "static", call.getClassName());

	}

	@Override
	public void visit(VirtualCall call) throws SemanticError {
		if (call.isExternal()){
			call.getLocation().accept(this);
		Type locationType = call.getLocation().getNodeType();
		if (locationType instanceof ClassType) {
			ICClass calledMethodClass = ((ClassType) locationType)
					.getClassNode();
			visitCall(call, "virtual", calledMethodClass.getName());
		} else
			throw new SemanticError(call.getLine(), String.format(
					"Cannot invoke method '%s' on type %s", call.getName(),
					locationType.getDescription()));
		}
		else
			visitCall(call, "virtual", call.getClassEnclosingScope().getId());
		

	}

	@Override
	public void visit(This thisExpression) throws SemanticError {
		thisExpression.setNodeType(globalTypeTable.getClassType(root
				.getClass(thisExpression.getClassEnclosingScope().getId())));

	}

	@Override
	public void visit(NewClass newClass) throws SemanticError {
		Type newType = globalTypeTable.getClassType(root.getClass(newClass
				.getName()));
		newClass.setNodeType(newType);

	}

	@Override
	public void visit(NewArray newArray) throws SemanticError {
		newArray.getType().accept(this);
		newArray.getSize().accept(this);

		Type arrType = newArray.getType().getNodeType();
		Type sizeType = newArray.getSize().getNodeType();

		if (!isIntType(sizeType))
			throw new SemanticError(newArray.getLine(),
					"array size must be of type int");

		newArray.setNodeType(globalTypeTable.getArrayType(arrType));

	}

	@Override
	public void visit(Length length) throws SemanticError {
		length.getArray().accept(this);
		Type t = length.getArray().getNodeType();
		if (!(t instanceof ArrayType))
			throw new SemanticError(length.getLine(),
					"'length' can only be used on an array type");
		length.setNodeType(globalTypeTable.getPrimType("int"));

	}

	@Override
	public void visit(MathBinaryOp binaryOp) throws SemanticError {
		binaryOp.getFirstOperand().accept(this);
		binaryOp.getSecondOperand().accept(this);
		Type firstOperandType = binaryOp.getFirstOperand().getNodeType();
		Type secondOperandType = binaryOp.getSecondOperand().getNodeType();
		boolean areOperandsInts = isIntType(firstOperandType)
				&& isIntType(secondOperandType);
		boolean areOperandsStrings = isStringType(firstOperandType)
				&& isStringType(secondOperandType);
		if (!areOperandsInts) {
			if (binaryOp.getOperator() != BinaryOps.PLUS)
				throw new SemanticError(binaryOp.getLine(), String.format(
						"%s can only be used on int types", binaryOp
								.getOperator().getDescription()));
			else if (!areOperandsStrings)
				throw new SemanticError(binaryOp.getLine(), String.format(
						"%s can only be used on int or string types (both operands types must match)", binaryOp
								.getOperator().getDescription()));
			binaryOp.setNodeType(firstOperandType);
		} else
			binaryOp.setNodeType(firstOperandType);

	}

	@Override
	public void visit(LogicalBinaryOp binaryOp) throws SemanticError {
		binaryOp.getFirstOperand().accept(this);
		binaryOp.getSecondOperand().accept(this);
		Type firstOperandType = binaryOp.getFirstOperand().getNodeType();
		Type secondOperandType = binaryOp.getSecondOperand().getNodeType();
		if (binaryOp.getOperator() == BinaryOps.EQUAL
				|| binaryOp.getOperator() == BinaryOps.NEQUAL) {
			verifyEqualOperator(binaryOp, firstOperandType, secondOperandType);
			binaryOp.setNodeType(globalTypeTable.getPrimType("boolean"));
		} else if (binaryOp.getOperator() == BinaryOps.LAND
				|| binaryOp.getOperator() == BinaryOps.LOR) {
			verifyAndOrOparator(binaryOp, firstOperandType, secondOperandType);
			binaryOp.setNodeType(globalTypeTable.getPrimType("boolean"));
		} else {
			verifyIntLogicalOperator(binaryOp, firstOperandType,
					secondOperandType);
			binaryOp.setNodeType(globalTypeTable.getPrimType("boolean"));
		}

	}

	private void verifyEqualOperator(LogicalBinaryOp binaryOp,
			Type firstOperandType, Type secondOperandType) throws SemanticError {
		if (!(firstOperandType.isSubtypeOf(secondOperandType) || secondOperandType
				.isSubtypeOf(firstOperandType)))
			throw new SemanticError(
					binaryOp.getLine(),
					String.format(
							"%s can only be used on matching types (including subtypes)",
							binaryOp.getOperator().getDescription()));
	}

	private void verifyAndOrOparator(LogicalBinaryOp binaryOp,
			Type firstOperandType, Type secondOperandType) throws SemanticError {
		if (!(isBooleanType(firstOperandType) && isBooleanType(secondOperandType)))
			throw new SemanticError(binaryOp.getLine(), String.format(
					"%s can only be used on boolean types", binaryOp
							.getOperator().getDescription()));
	}

	private void verifyIntLogicalOperator(LogicalBinaryOp binaryOp,
			Type firstOperandType, Type secondOperandType) throws SemanticError {
		if (!(isIntType(firstOperandType) && isIntType(secondOperandType)))
			throw new SemanticError(binaryOp.getLine(), String.format(
					"%s can only be used on int types", binaryOp.getOperator()
							.getDescription()));
	}

	@Override
	public void visit(MathUnaryOp unaryOp) throws SemanticError {
		unaryOp.getOperand().accept(this);
		Type operandType = unaryOp.getOperand().getNodeType();
		if (!isIntType(operandType))
			throw new SemanticError(unaryOp.getLine(), String.format(
					"%s can only be used on an int type", unaryOp.getOperator()
							.getDescription()));
		unaryOp.setNodeType(operandType);
	}

	@Override
	public void visit(LogicalUnaryOp unaryOp) throws SemanticError {
		unaryOp.getOperand().accept(this);
		Type operandType = unaryOp.getOperand().getNodeType();
		if (!isBooleanType(operandType))
			throw new SemanticError(unaryOp.getLine(), String.format(
					"%s can only be used on a boolean type", unaryOp
							.getOperator().getDescription()));
		unaryOp.setNodeType(operandType);
	}

	@Override
	public void visit(Literal literal) throws SemanticError {
		switch (literal.getType()) {
		case TRUE:
		case FALSE:
			literal.setNodeType(globalTypeTable.getPrimType("boolean"));
			break;
		case INTEGER:
			literal.setNodeType(globalTypeTable.getPrimType("int"));
			break;
		case STRING:
			literal.setNodeType(globalTypeTable.getPrimType("string"));
			break;
		case NULL:
			literal.setNodeType(globalTypeTable.getPrimType("null"));
			break;
		}
	}

	@Override
	public void visit(ExpressionBlock expressionBlock) throws SemanticError {
		expressionBlock.getExpression().accept(this);
		expressionBlock.setNodeType(expressionBlock.getExpression().getNodeType());

	}

	private boolean isBooleanType(Type t) {
		return t.isSubtypeOf(globalTypeTable.getPrimType("boolean"));
	}

	private boolean isIntType(Type t) {
		return t.isSubtypeOf(globalTypeTable.getPrimType("int"));
	}

	private boolean isStringType(Type t) {
		return t.isSubtypeOf(globalTypeTable.getPrimType("string"));
	}

}
